#!/bin/bash

usage() {
  echo "usage: $0 <packages_dir>"
  echo "  Ex: $0 /home/caspida/Splunk-UBA-<version>-Packages-RHEL-<version>"
}

INSTALLPATH="$1"
if [[ -z "${INSTALLPATH}" ]]; then
  echo "missing dir: needs the directory containing the packages as an argument"
  usage
  exit 2
fi

if [[ ! -d ${INSTALLPATH} ]]; then
  echo "specified directory not found: ${INSTALLPATH}"
  exit 3
fi

# some basic check
if [[ ! -f ${INSTALLPATH}/base_deps_list.txt ]]; then
  echo "missing ${INSTALLPATH}/base_deps_list.txt"
  echo "  make sure the specified directory:${INSTALLPATH} contains the Splunk UBA RHEL/CentOS packages"
  exit 4
fi

CASPIDAHOME=/home/caspida
VCAPSTOREDIR=/var/vcap/store
VCAPSYSLOGDIR=/var/vcap/sys/log
UBAUPGRADEDIR=/opt/caspida/upgrade
VARLOGDIR=/var/log
LOG=$VCAPSYSLOGDIR/caspida/install.log
ETCCASPIDADIR=/etc/caspida
CASPIDACONFDIR=/opt/caspida/conf
OPTCASPIDAINITD=/opt/caspida/etc/init.d
SPARKVER=spark-2.3.1-bin-hadoop2.6
KAFKAVER="kafka-0.11.0.1"
EXTRACONFDIR=$INSTALLPATH/extra_conf
EXTRAPACKAGESDIR=$INSTALLPATH/extra_packages
SPARKTGZ=$EXTRAPACKAGESDIR/tar/${SPARKVER}.tgz
KAFKATGZ=$EXTRAPACKAGESDIR/tar/${KAFKAVER}.tgz
PYTHON3="/opt/rh/rh-python36/root/bin/python3.6"
SUDO=sudo

_runcommand() {
  for command in "${@}"
  do
    eval ${command}
    if [ $? -ne 0 ]; then
      echo "Error in running ${command}, verify and re-run the INSTALL again"
      exit 1
    fi
  done
}

_makelink() {

  unset dstLink
  if [[ -h $3 ]]; then
    dstLink="true"
  fi

  command="${@}"
  eval ${command}

  for sourceLink in $2
  do
    s=`basename ${sourceLink}`
    if [[ "${dstLink}" = "true" ]]; then
      linkStr=$3/${s}
    elif [[ ! -h $3  &&  -d $3 ]]; then
      s=`basename ${sourceLink}`
      linkStr=$3/${s}
    else
      linkStr=$3
    fi

    echo "checking if link exists for ${linkStr}"
    readlink ${linkStr}
    #if [ $? -ne 0 ]; then
    #  echo "Problem creating symlink source=${sourceLink} destination=${3}"
    #  exit 2
    #fi
    echo "link exists ${sourceLink} ${linkStr}"
  done
}

echo "$(date): Starting $0 $@"

echo "#-------------------------"
echo "# $(date): Check caspida home        "
echo "#-------------------------"
echo ~caspida | awk '{ print $1 }'| grep "/home/caspida"
if [ $? -ne 0 ]; then
    echo "Set /home/caspida as home directory for caspida user and try again."
    exit 3
fi

echo "#-------------------------"
echo "# $(date): Create Directory  "
echo "#-------------------------"

id | grep root > /dev/null 2>&1
if [ $? -eq 0 ]; then
  SUDO=
fi

umask 0022

[ -e ~caspida/.bash_profile ] || ( touch ~caspida/.bash_profile && 
 chown caspida:caspida ~caspida/.bash_profile )

[ -e ~caspida/.bash_profile ] || ( touch ~caspida/.bashrc && 
 chown caspida:caspida ~caspida/.bash_profile )

( grep -q 'umask' ~caspida/.bash_profile &&
  sed -i -e 's/umask.*/umask 0022/' ~caspida/.bash_profile ) || 
  sed -i -e '$ a\umask 0022' ~caspida/.bash_profile

( grep -q 'umask' ~caspida/.bashrc &&
  sed -i -e 's/umask.*/umask 0022/' ~caspida/.bashrc ) || 
  sed -i -e '$ a\umask 0022' ~caspida/.bashrc

echo "*******************"
echo " $(date): VCAP DATA STORAGE "
echo "*******************"

$SUDO mkdir -p $VCAPSTOREDIR
for subdir in caspida flume flume-ng hadoop hadoop-hdfs hadoop-mapreduce hadoop-yarn kafka postgresql redis timeseries 
do
    i=0
    unset cmdArr
    cmdArr[((i++))]="$SUDO mkdir -p $VCAPSTOREDIR/$subdir"
    cmdArr[((i++))]="$SUDO chmod 755 $VCAPSTOREDIR/$subdir"
    _runcommand "${cmdArr[@]}"
done 

$SUDO ls -alt $VCAPSTOREDIR/*

echo "**********************"
echo " $(date): VCAP SYSLOG DIRECTORY"
echo "**********************"

i=0
unset cmdArr
cmdArr[((i++))]="$SUDO mkdir -p $VCAPSYSLOGDIR"
cmdArr[((i++))]="$SUDO chmod 755 $VCAPSYSLOGDIR"
cmdArr[((i++))]="$SUDO rm -fv /etc/docker/daemon.json"
cmdArr[((i++))]="$SUDO rm -rf /var/log/caspida"
cmdArr[((i++))]="$SUDO mkdir -p $VCAPSYSLOGDIR/caspida"
cmdArr[((i++))]="$SUDO chmod 755 $VCAPSYSLOGDIR/caspida"
cmdArr[((i++))]="$SUDO chown caspida:caspida $VCAPSYSLOGDIR/caspida"
_runcommand "${cmdArr[@]}"

currentdate=`date "+%Y-%m-%d-%H_%M_%S"`
exec > >(tee -a $LOG) 2>&1

# set the umask & print it
# print the current umask
echo "$(date): Running $0 $@"
echo "$(date): current umask=$(umask), id=$(id)"

echo "$(date): Installing Base Packages using sudo. [$SUDO yum -y install `echo "cat $INSTALLPATH/base_deps_list.txt"`]"
$SUDO yum -y install `cat $INSTALLPATH/base_deps_list.txt`
if [ $? -ne 0 ]; then
    echo "Unable to install Base Packages. Correct the errors and try again..."
    exit 4
fi

for subdir in zookeeper flume-ng hive hive-hcatalog impala hadoop-hdfs hadoop-mapreduce hadoop-yarn kafka postgresql redis influxdb
do
    i=0
    unset cmdArr
    cmdArr[((i++))]="$SUDO rm -rf /var/log/$subdir"
    cmdArr[((i++))]="$SUDO mkdir -p $VCAPSYSLOGDIR/$subdir"
    cmdArr[((i++))]="$SUDO chmod 755 $VCAPSYSLOGDIR/$subdir"
    _runcommand "${cmdArr[@]}"
done

i=0
unset cmdArr
cmdArr[((i++))]="$SUDO mkdir -m 755 -p /var/vcap/packages"
cmdArr[((i++))]="$SUDO chown caspida:caspida /var/vcap/packages"
cmdArr[((i++))]="$SUDO rm -f $ETCCASPIDADIR/conf"
cmdArr[((i++))]="$SUDO mkdir -p $ETCCASPIDADIR"
_runcommand "${cmdArr[@]}"

_makelink "$SUDO ln -sv" "$VCAPSYSLOGDIR/*" "$VARLOGDIR"
_makelink "$SUDO ln -sv" "$CASPIDACONFDIR" "$ETCCASPIDADIR"
_makelink "$SUDO ln -sv" "$OPTCASPIDAINITD/*" "/etc/init.d"
_makelink "$SUDO ln -sv" "$PYTHON3" "/usr/bin/python3"


echo "#-------------------------"
echo "# $(date): install BASE "
echo "#-------------------------"

line="******************\n"
echo $PWD

for dir in $INSTALLPATH/base_packages $EXTRAPACKAGESDIR/rpm/*
do
  if [[ ${dir} =~ kubernetes || ${dir} =~ docker || ${dir} =~ openssl ]]; then
    echo "${dir} will be installed during install_containerization.sh below"
    continue
  fi

  STRING=`ls $dir/*.rpm`

  if [[ ${dir} =~ "ruby" ]]; then
    PKGS=${STRING}
  else
    echo
    echo $(date): Checking the list of initial base packages before install : $STRING
    echo
    already_installed_newer=`$SUDO rpm -Uvh --test $STRING 2>&1 | grep "already installed" | grep "which is newer"`
    echo 
    PKGS=""
    for rpmname in $STRING
    do
      # get the rpm from string that has the following pattern /xyz/abc/i.rpm
      i=`echo $rpmname | awk -F "/" '{ print $NF }' | awk -F ".rpm" '{ print $1 }'`

      # Exception for java-cup
      echo $i | grep java_cup > /dev/null 2>&1
      if [ $? -eq 0 ]; then
        i="java_cup"
      fi

      # Exception for nodejs
      echo $i | grep "nodejs-[0-9]" > /dev/null 2>&1
      if [ $? -eq 0 ]; then
        i="nodejs-[0-9]"
      fi

      # Exception for nodejs-docs
      echo $i | grep "nodejs-docs-[0-9]" > /dev/null 2>&1
      if [ $? -eq 0 ]; then
        i="nodejs-docs"
      fi

      echo $already_installed_newer | grep $i  > /dev/null 2>&1
      if [ $? -eq 0 ]; then
        echo $rpmname is already installed and newer
      else
        PKGS="${PKGS} ${rpmname}"
      fi
    done
  fi

  if [[ ${dir} =~ "ruby" ]]; then
    $SUDO rpm -F $PKGS
  else
    echo $(date): Installing $PKGS with --replacepkgs
    $SUDO rpm -Uvh --replacepkgs --nodeps $PKGS
  fi

  if [ $? -ne 0 ]; then
    echo "$(date): Failed to install packages, correct the errors and try again. Exiting[2]..."
    exit 5
  fi
done

echo "#-------------------------"
echo "#$(date): ruby gems"
echo "#-------------------------"
$SUDO gem install --force --local $EXTRAPACKAGESDIR/gem/redis-3.3.3.gem
if [ $? -ne 0 ]; then
  echo "Failed to install redis-3.3.3.gem. Exiting."
  exit 6
fi

echo "#-------------------------"
echo "# $(date): initialize postgres"

i=0
unset cmdArr
cmdArr[((i++))]="$SUDO rm -rf /var/lib/pgsql"
cmdArr[((i++))]="$SUDO mkdir -p /var/lib/pgsql/10"
cmdArr[((i++))]="$SUDO chown postgres:postgres /var/lib/pgsql/10"
cmdArr[((i++))]="$SUDO chmod 700 /var/lib/pgsql/10"
_runcommand "${cmdArr[@]}"

RHEL_BASE_DIR=/usr/pgsql
RHEL_DATA_DIR=/var/lib/pgsql
VERSION=10
$SUDO -u postgres /usr/bin/initdb -D ${RHEL_DATA_DIR}/${VERSION}/data
if [ $? -ne 0 ]; then
  echo "Failed to initialize postgresql, correct the errors reported in the log and try again. Exiting..."
  exit 7
fi

echo "#-------------------------"
echo "# $(date): Disable X11Forwarding"
$SUDO grep -q "X11Forwarding yes" /etc/ssh/sshd_config && sudo sed -i "s/X11Forwarding yes/X11Forwarding no/" /etc/ssh/sshd_config
if [ $? -ne 0 ]; then
  echo "$(date): X11 Forwarding already disabled"
else
  $SUDO service sshd reload
fi
echo "#-------------------------"
echo "# $(date): Install extra package (tar)"
echo "#-------------------------"

tar xfz $EXTRAPACKAGESDIR/tar/pexpect-3.1.tar.gz -C $EXTRAPACKAGESDIR/tar
(cd $EXTRAPACKAGESDIR/tar/pexpect-3.1 && $SUDO python2 setup.py install)
if [ $? -ne 0 ]; then
  echo "$(date): Unable to install python expect packages. Exiting..."
  exit 8
fi

tar xfz $EXTRAPACKAGESDIR/tar/python-evtx-0.3.2.tar.gz -C $EXTRAPACKAGESDIR/tar
( cd $EXTRAPACKAGESDIR/tar/python-evtx-0.3.2 && $SUDO python2 setup.py install)
if [ $? -ne 0 ]; then
  echo "$(date): Unable to install python expect packages. Exiting..."
  exit 9
fi

tar xfz $EXTRAPACKAGESDIR/tar/python-pkgs.tgz -C $EXTRAPACKAGESDIR/tar
#$SUDO rpm -Uvh $EXTRAPACKAGESDIR/tar/python-pkgs/*.rpm

#i=0
#unset cmdArr
#cmdArr[((i++))]="rpm -q python2-six"
#cmdArr[((i++))]="rpm -q python2-dateutil"
#cmdArr[((i++))]="rpm -q python2-pysocks"
#cmdArr[((i++))]="rpm -q python2-urllib3"
#cmdArr[((i++))]="rpm -q python2-requests"
#cmdArr[((i++))]="rpm -q pytz"
#cmdArr[((i++))]="rpm -q python2-influxdb"
#_runcommand "${cmdArr[@]}"

i=0
unset cmdArr
cmdArr[((i++))]="$SUDO cp $INSTALLPATH/extra_conf/influxdb/* /etc/influxdb/."
cmdArr[((i++))]="$SUDO chmod 755 /etc/influxdb/influxdb.conf"
cmdArr[((i++))]="$SUDO mkdir -p /etc/hadoop/conf/"
cmdArr[((i++))]="$SUDO cp -r $INSTALLPATH/extra_conf/hadoop/* /etc/hadoop/conf/."
cmdArr[((i++))]="$SUDO mkdir -p /etc/impala/conf/"
cmdArr[((i++))]="$SUDO cp -r $INSTALLPATH/extra_conf/impala/*.xml /etc/impala/conf/."
cmdArr[((i++))]="$SUDO mkdir -p /var/vcap/packages"
cmdArr[((i++))]="$SUDO rm -rf /var/vcap/packages/spark"
cmdArr[((i++))]="$SUDO tar xfz $SPARKTGZ -C /var/vcap/packages"
cmdArr[((i++))]="$SUDO chown -R caspida:caspida /var/vcap/packages/spark*"
cmdArr[((i++))]="$SUDO chmod -R 755 /var/vcap/packages/spark-2.3.1-bin-hadoop2.6"
cmdArr[((i++))]="$SUDO rm -rf /usr/share/kafka"
cmdArr[((i++))]="$SUDO tar xfz $KAFKATGZ -C /usr/share"
cmdArr[((i++))]="$SUDO rm -rf /usr/share/kafka/site-docs/"
_runcommand "${cmdArr[@]}"

_makelink "$SUDO ln -sv" "/var/vcap/packages/$SPARKVER" "/var/vcap/packages/spark"
_makelink "$SUDO ln -sv" "/usr/share/$KAFKAVER" "/usr/share/kafka"

i=0
unset cmdArr
cmdArr[((i++))]="$SUDO chown -R caspida:caspida /usr/share/kafka"
_runcommand "${cmdArr[@]}"

_makelink "$SUDO ln -sv" "/var/vcap/sys/log/kafka" "/usr/share/kafka/logs"

echo "#-------------------------"
echo "# $(date): Update Spark"
echo "#-------------------------"

i=0
unset cmdArr
cmdArr[((i++))]="$SUDO cp /opt/caspida/conf/spark/* /var/vcap/packages/spark/conf/"
cmdArr[((i++))]="$SUDO chown caspida:caspida /var/vcap/packages/spark/conf/*"

cmdArr[((i++))]="perl -pi -e  's/#SPARK_WORKER_OPTS/SPARK_WORKER_OPTS/g' /var/vcap/packages/spark/conf/spark-env.sh"
#edit spark config
cmdArr[((i++))]="perl -pi -e  's/SPARK_EXECUTOR_MEMORY=.*\$/SPARK_EXECUTOR_MEMORY=2G/g' /var/vcap/packages/spark/conf/spark-env.sh"
cmdArr[((i++))]="perl -pi -e  's/#SPARK_WORKER_OPTS/SPARK_WORKER_OPTS/g' /var/vcap/packages/spark/conf/spark-env.sh"
cmdArr[((i++))]="$SUDO perl -pi -e  's/^bind .*$/bind 0\.0\.0\.0/g' /etc/redis.conf"
_runcommand "${cmdArr[@]}"

echo "#-------------------------"
echo "# $(date): Installing docker-ce and kubernetes"
echo "#-------------------------"
$UBAUPGRADEDIR/utils/install_containerization.sh $EXTRAPACKAGESDIR
if [[ $? -ne 0 ]]; then
  echo "Failed to install containerization, exiting..."
  exit 11
fi

i=0
unset cmdArr
cmdArr[((i++))]="$SUDO cp $EXTRACONFDIR/zookeeper/zoo.cfg /etc/zookeeper/conf/"
cmdArr[((i++))]="$SUDO cp $EXTRACONFDIR/hadoop/core-site.xml $EXTRACONFDIR/hadoop/hdfs-site.xml /etc/hadoop/conf/"
cmdArr[((i++))]="$SUDO cp $EXTRACONFDIR/hive/core-site.xml $EXTRACONFDIR/hive/hdfs-site.xml $EXTRACONFDIR/hive/hive-site.xml /etc/hive/conf/"
cmdArr[((i++))]="$SUDO cp $EXTRACONFDIR/impala/core-site.xml $EXTRACONFDIR/impala/hdfs-site.xml $EXTRACONFDIR/impala/hive-site.xml /etc/impala/conf/"
cmdArr[((i++))]="$SUDO chown root:hadoop /etc/hadoop/conf/core-site.xml /etc/hadoop/conf/hdfs-site.xml"
cmdArr[((i++))]="$SUDO chmod 644 /etc/hadoop/conf/core-site.xml /etc/hadoop/conf/hdfs-site.xml"
cmdArr[((i++))]="$SUDO chown root:root /etc/zookeeper/conf/zoo.cfg"
cmdArr[((i++))]="$SUDO chmod 644 /etc/zookeeper/conf/zoo.cfg"
cmdArr[((i++))]="$SUDO chown root:root /etc/hive/conf/hive-site.xml"
cmdArr[((i++))]="$SUDO chmod 644 /etc/hive/conf/hive-site.xml"
cmdArr[((i++))]="$SUDO chown root:root /etc/impala/conf/core-site.xml /etc/impala/conf/hdfs-site.xml /etc/impala/conf/hive-site.xml"
cmdArr[((i++))]="$SUDO chmod 644 /etc/impala/conf/core-site.xml /etc/impala/conf/hdfs-site.xml /etc/impala/conf/hive-site.xml"
cmdArr[((i++))]="$SUDO mkdir -p /var/lib/zookeeper"
cmdArr[((i++))]="$SUDO chown -R zookeeper:zookeeper /var/lib/zookeeper"
cmdArr[((i++))]="$SUDO chmod 755 /var/lib/zookeeper"
_runcommand "${cmdArr[@]}"

_makelink "$SUDO ln -svf" "/etc/localtime" "/usr/share/zoneinfo"

$SUDO zookeeper-server stop
$SUDO zookeeper-server init --force
$SUDO zookeeper-server start
sleep 1
$SUDO zookeeper-server status
#if [ $? -ne 0 ]; then
#  echo "$(date): Failed to start zookeeper-server, correct the errors reported in the log and try again. Exiting..."
#  exit 12
#fi
$SUDO zookeeper-server stop

i=0
unset cmdArr
cmdArr[((i++))]="$SUDO rm -rf /var/vcap/store/redis /var/vcap/store/pgsql /var/lib/redis"
cmdArr[((i++))]="$SUDO mv /var/lib/pgsql /var/vcap/store/"
cmdArr[((i++))]="$SUDO chown postgres:postgres /var/vcap/store/pgsql"
cmdArr[((i++))]="$SUDO mkdir -p /var/vcap/store/redis"
cmdArr[((i++))]="$SUDO chown redis:redis /var/vcap/store/redis"
cmdArr[((i++))]="$SUDO usermod -a -G redis caspida"
cmdArr[((i++))]="$SUDO rm -rf /var/lib/kafka"
cmdArr[((i++))]="$SUDO rm -rf /var/lib/hadoop-yarn"
cmdArr[((i++))]="$SUDO rm -rf /var/lib/hadoop-hdfs"
cmdArr[((i++))]="$SUDO rm -rf /var/lib/hadoop-mapreduce"
cmdArr[((i++))]="$SUDO rm -rf /usr/lib/hive/lib/postgresql-jdbc4.jar"
_runcommand "${cmdArr[@]}"

_makelink "$SUDO ln -sv" "/usr/share/java/postgresql-jdbc.jar" "/usr/lib/hive/lib/postgresql-jdbc4.jar"
_makelink "$SUDO ln -sv" "/var/vcap/store/pgsql" "/var/lib/pgsql"
_makelink "$SUDO ln -sv" "/var/vcap/store/redis" "/var/lib/redis"
_makelink "$SUDO ln -sv" "/var/vcap/store/kafka" "/var/lib/kafka"
_makelink "$SUDO ln -sv" "/var/vcap/store/hadoop-yarn" "/var/lib/hadoop-yarn"
_makelink "$SUDO ln -sv" "/var/vcap/store/hadoop-hdfs" "/var/lib/hadoop-hdfs"
_makelink "$SUDO ln -sv" "/var/vcap/store/hadoop-mapreduce" "/var/lib/hadoop-mapreduce"

$SUDO chkconfig --list | \
      egrep 'flume|hadoop|hive|impala|postgres|redis|zookeeper|influxdb' | \
      awk '{ print $1 }' > /tmp/serv.txt

for i in `cat /tmp/serv.txt`
do
  echo $i
  $SUDO chkconfig $i --levels 2345 off
done
$SUDO rm /tmp/serv.txt

i=0
unset cmdArr
cmdArr[((i++))]="$SUDO chmod -R 755 /opt/caspida"
cmdArr[((i++))]="$SUDO chown -R caspida:caspida /opt/caspida"
cmdArr[((i++))]="sudo cp -nv /usr/lib/flume/bin/flume-ng /usr/lib/flume/bin/flume-ng.ORIG"
cmdArr[((i++))]="sudo cp -v /opt/caspida/bin/flume/flume-ng /usr/lib/flume/bin/flume-ng"
cmdArr[((i++))]="sudo chmod 755 /usr/lib/flume/bin/flume-ng"
_runcommand "${cmdArr[@]}"

i=0
unset cmdArr
echo "$(date): Running CreateCaspidaSecurityJar.py"
cmdArr[((i++))]="python2  /opt/caspida/bin/CreateCaspidaSecurityJar.py"
cmdArr[((i++))]="$SUDO chown caspida:caspida /opt/caspida/lib/CaspidaSecurity.jar"
cmdArr[((i++))]="$SUDO mkdir -p /etc/caspida/local/conf"
cmdArr[((i++))]="$SUDO chmod -R 755 /etc/caspida"
cmdArr[((i++))]="$SUDO chown -R caspida:caspida /etc/caspida/local"
cmdArr[((i++))]="$SUDO mkdir -p /var/vcap/sys/run"
cmdArr[((i++))]="$SUDO chmod 755 /var/vcap/sys/run"
cmdArr[((i++))]="cp /opt/caspida/conf/deployment/templates/local_conf/uba-site.properties /etc/caspida/local/conf/"
cmdArr[((i++))]="$SUDO chmod 644 /usr/lib/python2.7/site-packages/*.egg"
_runcommand "${cmdArr[@]}"

echo "$(date): $0 completed...."
